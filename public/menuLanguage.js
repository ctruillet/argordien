let flag_fr;
let flag_en;
let flag_de;
let flag_es;


let LANGUAGE = 0;
/* LANGUAGE
*    0 = FR
*    1 = EN
*    2 = DE
*    3 = ES
*/


function drawMenuLanguage(){
  tint(160);
  image(flag_fr,10,10);
  image(flag_en,10,48);
  image(flag_de,10,86);
  image(flag_es,10,124);
  
  noTint();
  
  switch(LANGUAGE){
    case 0:
      image(flag_fr,10,10);
      break;
    
    case 1:
      image(flag_en,10,48);
      break;
    
    case 2:
      image(flag_de,10,86);
      break;
    
    case 3:
      image(flag_es,10,124);
      break;
    
    default:
      image(flag_fr,10,10);
  }
  
}

function changeLanguage(input){
  switch(input){
      case "!FR":
        LANGUAGE=0;
        break;
      
      case "!EN":
        LANGUAGE=1;
        break;
      
      case "!DE":
        LANGUAGE=2;
        break;
        
      case "!ES":
        LANGUAGE=3;
        break;
      
      default:
        LANGUAGE=0;
    }
    
    document.getElementById('message').value ='';
    print("Set Language to " + LANGUAGE);
}


function mousePressed(){
  if(mouseX>=10 && mouseX<=46  ){
    
    //FR FLAG
    if(mouseY>=10 && mouseY <=38 ){
      LANGUAGE=0;
      isNeedDisplayAgainMessageOutout = true;
    
    //EN FLAG
    }else if(mouseY>=48 && mouseY <=76 ){
      LANGUAGE=1;
      isNeedDisplayAgainMessageOutout = true;
      
    //DE FLAG  
    }else if(mouseY>=86 && mouseY <=114 ){
      LANGUAGE=2;
      isNeedDisplayAgainMessageOutout = true;
      
    //ES FLAG  
    }else if(mouseY>=124 && mouseY <=152 ){
      LANGUAGE=3;
      isNeedDisplayAgainMessageOutout = true;
    }
    
  }
  
}
