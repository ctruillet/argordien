let data = {};

let data_fr = {};
let data_en = {};
let data_de = {};
let data_es = {};

let isNeedDisplayAgainMessageOutout = true;

let input;
let listOfDefinition = "";

let listOfResearch = [];

function preload() {
  flag_fr = loadImage("./img/flag/flag_fr.png");
  flag_en = loadImage("./img/flag/flag_en.png");
  flag_de = loadImage("./img/flag/flag_de.png");
  flag_es = loadImage("./img/flag/flag_es.png");

  data_fr = loadJSON("./data_fr.json");
  data_en = loadJSON("./data_en.json");
  data_de = loadJSON("./data_de.json");
  data_es = loadJSON("./data_es.json");

  let params = getURLParams();
  if (typeof params.t  !== 'undefined' && params.t  !=  ""){
    document.getElementById('message').value = params.t;
  }
}

function sendEventResearch(input){
  // Check if input is the last research
  if (listOfResearch.length > 0 && listOfResearch[listOfResearch.length - 1] == input){
    return;
  }

  listOfResearch.push(input);

  _mtm.push({"event": "search"});

  // _paq.push(['trackEvent', 'Search', 'Abréviation', input]);
}


function setup() {
  var cnv = createCanvas(56, 162);
  cnv.parent("canvas");
}

function draw() { 
  drawMenuLanguage();
  
  listOfDefinition = "";

 
  input = document.getElementById('message').value.toUpperCase();
  input = input.replace(/ /g,'');
  
  if(match(input,'^![FR|DE|EN|ES]{2}') != null){
    changeLanguage(input);
    isNeedDisplayAgainMessageOutout = true;
  }
  
  
  //Choix du bon jeu de donnée
  switch(LANGUAGE){
    case 0:
      data = data_fr;
      break;
    
    case 1:
      data = data_en;
      break;
    
    case 2:
      data = data_de;
      break;
    
    case 3:
      data = data_es;
      break;
    
    default:
      data = data_fr;
  }
  
  //Recherche de la definition
  var abreviations = data.data;
  
  for(var i = 0; i<abreviations.length ; i++){
    if(abreviations[i].word === input){
      addDefinition(abreviations[i]);
    }
  }
  
  
  if(listOfDefinition != ""){
    //Afficher la/les definitions
    document.getElementById('output').innerHTML = listOfDefinition;
    isNeedDisplayAgainMessageOutout = true;
    document.title = "L'Argordien - " + input;
    
    

    
    
    // Change the url displayed in the browser
    // history.replaceState(null, null, "?t=" + input);
    
    // Send event to matomo
    sendEventResearch(input);
 
  }else{
    //Afficher message présentation
    if (isNeedDisplayAgainMessageOutout){
      displayMessageOutput();
      isNeedDisplayAgainMessageOutout = false;
    }
  }
}
