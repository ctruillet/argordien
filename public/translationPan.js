function displayMessageOutput(){
  switch(LANGUAGE){
      //FR
      // case 0:
      //   document.getElementById('output').innerHTML = "<strong>L\'Argordien</strong> est un site développé par <em><a href=\"https://myhordes.eu/jx/soul/184\" target=\"_blank\"><u>Tache</u></a></em> pour <em>Hordes</em>.<br />Rentrez l\'abréviation que vous ne comprenez pas <s>et tapez (fort) sur <em>Entrée</em></s> !<br /><br />Certaines images appartiennent à <em><a href=\"https://motion-twin.com/fr/team\">Motion Twin</a></em><br />";   
      //   document.getElementById('message').placeholder = "Ecrivez votre abréviation ici !";
      //   break;
      
      //EN
      case 1:
        document.getElementById('output').innerHTML = "<strong>Argordien</strong> is a site developed by <em><a href=\"https://myhordes.eu/jx/soul/184\" target=\"_blank\"><u>Teasch</u></a></em> for <em>MyHordes</em>.<br />Enter the abbreviation you don\'t understand <s>and hit (hard) your <em>Space Bar</em></s> !<br /><br />Some images belong to <em><a href=\"https://motion-twin.com/fr/team\">Motion Twin</a></em><br />";   
        document.getElementById('message').placeholder = "Write your abbreviation here!";
        break;
      
      //DE
      case 2:
        document.getElementById('output').innerHTML = "Die <strong>Argordien</strong> ist eine von <em><a href=\"https://myhordes.eu/jx/soul/184\" target=\"_blank\"><u>Teasch</u></a></em> für <em>„MyHordes“</em> entwickelte Website.<br />Wenn Sie eine Abkürzung nicht verstehen, geben Sie diese hier ein <s>und drücken Sie die <em>Enter-Taste</em>!</s><br /><br />Einige Symbole gehören zu <em><a href=\"https://motion-twin.com/fr/team\">Motion Twin</a></em><br />";   
        document.getElementById('message').placeholder = "Schreiben Sie hier Ihre Abkürgung!";
        break;
        
      //ES
      case 3:
        document.getElementById('output').innerHTML = "<strong>L\'Argordien</strong> es un sitio desarrollado por <em><a href=\"https://myhordes.eu/jx/soul/184\" target=\"_blank\"><u>Teasch</u></a></em> para <em>MyHordes</em>.<br />Introduce la abreviatura que no entiendes <s>y pulsa (en voz alta) en <em>Enter</em>!</s> !<br /><br />Algunas imágenes pertenecen a <em><a href=\"https://motion-twin.com/fr/team\">Motion Twin</a></em><br />";   
        document.getElementById('message').placeholder = "¡Escriba su abreviatura aquí!";
        break;
      
      // FR and default  
      default:
        document.getElementById('output').innerHTML = "<strong>L\'Argordien</strong> est un site développé par <em><a href=\"https://myhordes.eu/jx/soul/184\" target=\"_blank\"><u>Teasch</u></a></em> pour <em>MyHordes</em>.<br />Rentrez l\'abréviation que vous ne comprenez pas <s>et tapez (fort) sur <em>Entrée</em></s> !<br /><br />Certaines images appartiennent à <em><a href=\"https://motion-twin.com/fr/team\">Motion Twin</a></em><br />";   
        document.getElementById('message').placeholder = "Ecrivez votre abréviation ici !";
    }
  
}


function addDefinition(abrv){ 
  //Image
    
  listOfDefinition+="<img src=\"img/";  
  listOfDefinition+=abrv.icon;
  // switch(abrv.category){
  //   case "Joueur":
  //     listOfDefinition+="joueur.gif";
  //     break;
     
  //   case"Forum":
  //     listOfDefinition+="forum.gif";
  //   break;
     
  //   case"Regroupement":
  //     listOfDefinition+="regroupement.gif";
  //     break;
    
  //   case"Arme":
  //     listOfDefinition+="arme.gif";
  //     break;
    
  //   case"Objet Encombrant":
  //     listOfDefinition+="objetEncombrant.gif";
  //     break;
    
  //   case"Objet":
  //     listOfDefinition+="objet.gif";
  //     break;
    
  //   case"Nourriture":
  //     listOfDefinition+="nourriture.gif";
  //     break;
    
  //   case"Drogue":
  //     listOfDefinition+="drogue.gif";
  //     break;
    
  //   case"Action":
  //     listOfDefinition+="action.gif";
  //     break;
    
  //   case"Gameplay":
  //     listOfDefinition+="gameplay.gif";
  //     break;
    
  //   case"Heros":
  //     listOfDefinition+="heros.gif";
  //     break;
    
  //   case"Batiment":
  //     listOfDefinition+="batiment.gif";
  //     break;
    
  //   case"Application":
  //     listOfDefinition+="application.gif";
  //     break;
    
  //   case"Event":
  //     listOfDefinition+="event.gif";
  //     break;
    
  //   case"Chantier":
  //     listOfDefinition+="chantier.gif";
  //     break;
    
  //   case"Alcool":
  //     listOfDefinition+="alcool.gif";
  //     break;
      
  //   default:
  //     listOfDefinition+="aa.gif";
     
  // }
  
  listOfDefinition+="\"> ";  
  
  //Signification
  listOfDefinition+="<strong>"+abrv.meaning+"</strong> : ";
  
  
  //Catégorie
  //listOfDefinition+="<em>"+abrv.category+"</em> ";  
  //Description
  listOfDefinition+=""+abrv.description+"";  
  
  listOfDefinition+="<br/>";
}
